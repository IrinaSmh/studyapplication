package tech.visdom.study_app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.visdom.study_app.dao.*;
import tech.visdom.study_app.dto.AnswerDto;
import tech.visdom.study_app.dto.CommentDto;
import tech.visdom.study_app.dto.MarkDto;
import tech.visdom.study_app.dto.ScheduleDto;
import tech.visdom.study_app.dto.TaskDto;
import tech.visdom.study_app.exception.AnswerNotFoundException;
import tech.visdom.study_app.exception.CommentNotFoundException;
import tech.visdom.study_app.exception.LessonNotFoundException;
import tech.visdom.study_app.exception.TaskNotFoundException;
import tech.visdom.study_app.model.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {
    private TaskDao taskDao;
    private LessonDao lessonDao;
    private AnswerDao answerDao;
    private CommentDao commentDao;
    private MarkDao markDao;

    @Autowired
    public TaskService(TaskDao taskDao, LessonDao lessonDao, AnswerDao answerDao, CommentDao commentDao, MarkDao markDao) {
        this.taskDao = taskDao;
        this.lessonDao = lessonDao;
        this.answerDao = answerDao;
        this.commentDao = commentDao;
        this.markDao = markDao;
    }

    @Transactional
    public TaskDto saveTask(TaskDto taskDto){
        lessonDao.findById(taskDto.getLessonId()).orElseThrow(() -> new LessonNotFoundException("Lesson with id " + taskDto.getLessonId() + " not found."));
        return new TaskDto(taskDao.save(new Task(taskDto)));
    }

    public TaskDto getTaskById(Long id){
        return new TaskDto(taskDao.findById(id).orElseThrow(() -> new TaskNotFoundException("Task with id " + id + " not found.")));
    }

    @Transactional
    public AnswerDto saveAnswer(AnswerDto answerDto) {
        taskDao.findById(answerDto.getTaskId()).orElseThrow(() -> new TaskNotFoundException("Task with id " + answerDto.getTaskId() + " not found."));
        if (answerDao.findByTaskIdAndUser_Id(answerDto.getTaskId(), answerDto.getStudentDto().getId()).isEmpty())
            return new AnswerDto(answerDao.save(new Answer(answerDto)));
        else
        {
            answerDao.update(answerDto.getTaskId(),
                    answerDto.getStudentDto().getId(), answerDto.getComment());
            return answerDto;
        }



    }

    public AnswerDto getAnswerById(Long id){
        return new AnswerDto(answerDao.findById(id).orElseThrow(() -> new AnswerNotFoundException("Answer with id " + id + " not found.")));
    }

    public CommentDto saveComment(String comment, Long taskId, Long userId){
        taskDao.findById(taskId).orElseThrow(()-> new TaskNotFoundException("Task with id " +taskId+ " not found."));
        Comment com = commentDao.save(new Comment(comment, taskId, userId));
        return new CommentDto(com);
    }

    public List<CommentDto> getAllCommentsOFTask(Long taskId){
        List<Comment> result = commentDao.findAllByTaskId(taskId).orElseThrow(()-> new CommentNotFoundException("Comments for task with id  " +taskId+ " not found."));
        return result.stream().map(CommentDto::new).collect(Collectors.toList());
    }

    public MarkDto saveMark(MarkDto markDto) {
        return new MarkDto(markDao.save(new Mark(markDto)));
    }

    public AnswerDto getByTaskIdAndStudentId(Long taskId, Long studentId) {
        return new AnswerDto(answerDao.findByTaskIdAndUser_Id(taskId, studentId)
                .orElseThrow(() -> new AnswerNotFoundException("Answer with taskId " + taskId + "and studentId " + studentId+" not found.")));
    }

    public List<AnswerDto> getStudentsAndAnswersByTaskId(Long taskId) {
        List<Answer> answers = answerDao.findAllByTaskId(taskId)
                .orElseThrow(() -> new AnswerNotFoundException("No answers to task with taskId " + taskId));
        return answers.stream().map(AnswerDto::new).collect(Collectors.toList());

    }
}
