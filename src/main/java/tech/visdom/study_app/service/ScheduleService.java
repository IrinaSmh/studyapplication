package tech.visdom.study_app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.visdom.study_app.dao.ScheduleDao;
import tech.visdom.study_app.dao.UserDao;
import tech.visdom.study_app.dto.*;
import tech.visdom.study_app.exception.ScheduleNotFoundException;
import tech.visdom.study_app.exception.UserNotFoundException;
import tech.visdom.study_app.model.Schedule;
import tech.visdom.study_app.model.User;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScheduleService {

    ScheduleDao scheduleDao;
    UserDao userDao;

    @Autowired
    public ScheduleService(ScheduleDao scheduleDao, UserDao userDao) {
        this.scheduleDao = scheduleDao;
        this.userDao = userDao;
    }

    public ScheduleDto getScheduleById(Long id) {
        Schedule schedule = scheduleDao.findById(id).orElseThrow(() -> new ScheduleNotFoundException("Invalid schedule id"));
        return new ScheduleDto(schedule.getId(),
                new ClassDto(schedule.getClasss()),
                new LessonDto(schedule.getLesson()),
                new TeacherDto(schedule.getTeacher()),
                new SubjectDto(schedule.getSubject()));
    }

    public List<ScheduleDto> getTeacherScheduleFromDateToDate(Long teacherId, LocalDate fromDate, LocalDate toDate) {
        User teacher = userDao.findById(teacherId).orElseThrow(()->new UserNotFoundException("No teacher with this id"));
        List<Schedule> schedules = scheduleDao.findAllByTeacherAndLessonDateBetween(teacher, fromDate, toDate)
                .orElseThrow(() -> new ScheduleNotFoundException("!!!!!!"));
        return schedules.stream().map(ScheduleDto::new).collect(Collectors.toList());

    }
}
