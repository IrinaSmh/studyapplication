package tech.visdom.study_app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tech.visdom.study_app.dao.UserDao;
import tech.visdom.study_app.dto.UserDto;
import tech.visdom.study_app.exception.UserNotFoundException;
import tech.visdom.study_app.model.User;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Arrays;

import java.util.stream.Collectors;

@Service
public class UserService {

    private UserDao userDao;

    private PasswordEncoder passwordEncoder;


    @Autowired
    public UserService(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto getAuthUserCredentials() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new UserDto(getUserByLogin(userDetails.getUsername()));
    }

    public User getUserByLogin(String login) {
        return userDao.findByLogin(login).orElseThrow(() -> new UsernameNotFoundException("Invalid login"));
    }

    public UserDto save(UserDto userDto) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        return new UserDto(userDao.save(new User(userDto)));
    }


    @Transactional
    public void delete(Long id) {
        userDao.deleteById(id);
    }

    public UserDto getById(Long id) throws UserNotFoundException {
        return new UserDto(userDao.findById(id).orElseThrow(() -> new UserNotFoundException("User with " + id + " not found")));
    }

    public void updatePassword(String password, Long userId){
        User user = userDao.findById(userId).orElseThrow(() -> new UserNotFoundException("User with " + userId + " not found"));
        user.setPassword(passwordEncoder.encode(password));
        userDao.save(user);
    }


}
