package tech.visdom.study_app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.visdom.study_app.dto.AnswerDto;
import tech.visdom.study_app.exception.AnswerNotFoundException;
import tech.visdom.study_app.exception.TaskNotFoundException;
import tech.visdom.study_app.service.TaskService;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/answers")
public class AnswerController {

    private TaskService taskService;

    @Autowired
    public AnswerController(TaskService taskService) {
        this.taskService = taskService;
    }


    @GetMapping("/getById/{id}")
    public ResponseEntity<AnswerDto> getAnswerById(@PathVariable(value = "id") Long id){
        try {
            return new ResponseEntity<>(taskService.getAnswerById(id), HttpStatus.OK);
        } catch (AnswerNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<AnswerDto> save(@RequestBody @Valid AnswerDto answerDto){
        try {
            return new ResponseEntity<>(taskService.saveAnswer(answerDto), HttpStatus.CREATED);
        } catch (TaskNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByTaskIdAndStudentId/{taskId}/{studentId}")
    public ResponseEntity<AnswerDto> getByTaskIdAndStudentId(@PathVariable(value = "taskId") Long taskId, @PathVariable(value = "studentId") Long studentId){
        try {
            return new ResponseEntity<>(taskService.getByTaskIdAndStudentId(taskId, studentId), HttpStatus.OK);
        } catch (AnswerNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getStudentsAndAnswersByTaskId/{taskId}")
    public ResponseEntity<List<AnswerDto>> getStudentsAndAnswersByTaskId(@PathVariable(value = "taskId") Long taskId){
        try {
            return new ResponseEntity<>(taskService.getStudentsAndAnswersByTaskId(taskId), HttpStatus.OK);
        } catch (AnswerNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }






}
