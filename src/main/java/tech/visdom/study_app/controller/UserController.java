package tech.visdom.study_app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import tech.visdom.study_app.dto.UserDto;
import tech.visdom.study_app.exception.UserNotFoundException;
import tech.visdom.study_app.service.UserService;


import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public ResponseEntity getExtendedUserInfo() {
        try {
            return new ResponseEntity<>(userService.getAuthUserCredentials(), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/user/{login}")
    public UserDto getAuthUserCredentials(@PathVariable(value = "login") String login) {
        return new UserDto(userService.getUserByLogin(login));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDto> getById(@PathVariable(value = "id") Long id) {
        try{
            return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
        }catch (UserNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/signup")
    public UserDto save(@RequestBody UserDto userDto) {
        userDto = userService.save(userDto);
        return userDto;
    }

    @Secured(value = "ROLE_ADMIN")
    @PutMapping(value = "/password")
    public void updatePassword(String password) {
        userService.updatePassword(password, userService.getAuthUserCredentials().getId());
    }

    @Secured(value = "ROLE_ADMIN")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try{
            userService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }catch (UserNotFoundException e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }


}
