package tech.visdom.study_app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.visdom.study_app.dto.ScheduleDto;
import tech.visdom.study_app.dto.UserDto;
import tech.visdom.study_app.exception.ScheduleNotFoundException;
import tech.visdom.study_app.exception.UserNotFoundException;
import tech.visdom.study_app.model.Schedule;
import tech.visdom.study_app.service.ScheduleService;
import tech.visdom.study_app.service.UserService;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/teachers")
public class ScheduleController {

    ScheduleService scheduleService;

    UserService userService;

    @Autowired
    public ScheduleController(ScheduleService scheduleService, UserService userService) {
        this.scheduleService = scheduleService;
        this.userService = userService;
    }

    //@Secured(value = "Teacher")
    @GetMapping(value = "schedule/{id}")
    public ResponseEntity<ScheduleDto> getScheduleById(@PathVariable(value = "id") Long id) {
        try{
            return new ResponseEntity<>(scheduleService.getScheduleById(id), HttpStatus.OK);
        }catch (ScheduleNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //@Secured(value = "Teacher")
    @GetMapping(value = "schedule/{fromDate}/{toDate}")
    public ResponseEntity<List<ScheduleDto>> getTeacherScheduleFromDateToDate(@PathVariable(value = "fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                                                              @PathVariable(value = "toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate) {
        try{
            return new ResponseEntity<>(scheduleService.getTeacherScheduleFromDateToDate(userService.getAuthUserCredentials().getId(), fromDate, toDate), HttpStatus.OK);
        }catch (ScheduleNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }




}
