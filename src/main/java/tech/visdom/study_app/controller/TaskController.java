package tech.visdom.study_app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import tech.visdom.study_app.dto.CommentDto;
import tech.visdom.study_app.dto.MarkDto;
import tech.visdom.study_app.dto.TaskDto;
import tech.visdom.study_app.exception.CommentNotFoundException;
import tech.visdom.study_app.exception.LessonNotFoundException;
import tech.visdom.study_app.exception.TaskNotFoundException;
import tech.visdom.study_app.service.TaskService;
import tech.visdom.study_app.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/tasks")
public class TaskController {
    private TaskService taskService;
    private UserService userService;

    @Autowired
    public TaskController(TaskService taskService, UserService userService) {
        this.taskService = taskService;
        this.userService = userService;
    }

   // @Secured(value = "Teacher")
    @PostMapping("/save")
    public ResponseEntity<TaskDto> save(@RequestBody @Valid TaskDto taskDto){
            return new ResponseEntity<>(taskService.saveTask(taskDto), HttpStatus.CREATED);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable(value = "id") Long id){
        try {
            return new ResponseEntity<>(taskService.getTaskById(id), HttpStatus.OK);
        } catch (TaskNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/saveComment/{id}")
    public ResponseEntity<CommentDto> saveComment(@PathVariable(value = "id") @Valid Long taskId, String comment){
            return new ResponseEntity<>(taskService.saveComment(comment, taskId, userService.getAuthUserCredentials().getId()), HttpStatus.CREATED);
    }

    @GetMapping("/getCommentsByTaskId/{id}")
    public ResponseEntity<List<CommentDto>> getAllCommentsByTaskId(@PathVariable(value = "id") Long taskId){
        try{
            return new ResponseEntity<>(taskService.getAllCommentsOFTask(taskId), HttpStatus.OK);
        } catch (CommentNotFoundException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/setMark")
    public ResponseEntity<MarkDto> setMark(@RequestBody @Valid MarkDto markDto){
        return new ResponseEntity<>(taskService.saveMark(markDto), HttpStatus.CREATED);
    }


}
