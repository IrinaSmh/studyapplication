package tech.visdom.study_app.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.visdom.study_app.dto.UserDto;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "LOGIN"))
public class User {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name = "NAME", nullable = false)
    private String name;

    @Getter
    @Setter
    @Column(name = "SURNAME", nullable = false)
    private String surName;

    @Getter
    @Setter
    @Column(name = "SECONDNAME", nullable = false)
    private String secondName;

    @Getter
    @Setter
    @Column(name = "LOGIN", nullable = false)
    private String login;

    @Getter
    @Setter
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "PASSWORD", nullable = false)
    private String password;


    @Getter
    @Setter
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "USER_ROLE",
            joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")
    )
    private List<Role> roles;

    @OneToMany(mappedBy="user", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Answer> answerSet;

    @OneToMany(mappedBy="teacher", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Schedule> scheduleSet;

    public User(UserDto userDto) {
        id = userDto.getId();
        name = userDto.getName();
        surName = userDto.getSurName();
        secondName = userDto.getSecondName();
        login = userDto.getLogin();
        password = userDto.getPassword();
        roles = userDto.getRoles();
    }

    public User(Long id)
    {
        this.id = id;
    }

}
