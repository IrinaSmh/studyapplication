package tech.visdom.study_app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.visdom.study_app.dto.LessonDto;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table
public class Lesson {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    @Column(name = "DATE")
    private LocalDate date;

    @Getter
    @Setter
    @Column(name = "TIME")
    private LocalTime time;

    @OneToMany(mappedBy="lesson", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Schedule> scheduleSet;

    public Lesson(LessonDto lessonDto) {
       this.date = lessonDto.getDate();
       this.time = lessonDto.getTime();
    }
}
