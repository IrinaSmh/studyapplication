package tech.visdom.study_app.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.dto.CommentDto;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "COMMENT")
@Data
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "STRING")
    private String string;

    @Column(name = "TASK_ID")
    private Long taskId;

    @Column(name = "USER_ID")
    private Long userId;

    public Comment(CommentDto commentDto) {
        this.id = commentDto.getId();
        this.string = commentDto.getString();
        this.taskId = commentDto.getTaskId();
        this.userId = commentDto.getUserId();
    }

    public Comment(String string, Long taskId, Long userId) {
        this.string = string;
        this.taskId = taskId;
        this.userId = userId;
    }
}
