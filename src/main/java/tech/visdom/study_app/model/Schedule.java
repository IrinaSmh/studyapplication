package tech.visdom.study_app.model;

import lombok.*;

import javax.persistence.*;


@NoArgsConstructor
@Entity
@Table(name="SCHEDULE")
@ToString
@Data
public class Schedule {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="CLASS_ID", nullable=false)
    private Class classs;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="LESSON_ID", nullable=false)
    private Lesson lesson;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="SUBJECT_ID", nullable=false)
    private Subject subject;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="TEACHER_ID", nullable=false)
    private User teacher;



}
