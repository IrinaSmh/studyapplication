package tech.visdom.study_app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table
public class Class {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    @Column(name = "NUMBER")
    private Integer number;

    @Getter
    @Setter
    @Column(name = "LETTER", length = 63)
    private String letter;

    @Getter
    @Setter
    @Column(name = "YEAR")
    private Integer year;

    @OneToMany(mappedBy="classs", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Schedule> scheduleSet;


}
