package tech.visdom.study_app.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.dto.AnswerDto;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table(name = "ANSWER")
@Data
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "TASK_ID")
    private Long taskId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="STUDENT_ID", nullable=false)
    private User user;

    public Answer(AnswerDto answerDtо) {
        this.id = answerDtо.getId();
        this.comment = answerDtо.getComment();
        this.taskId = answerDtо.getTaskId();
        this.user = new User(answerDtо.getStudentDto().getId());
    }
}
