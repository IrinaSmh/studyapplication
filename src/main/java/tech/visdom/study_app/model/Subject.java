package tech.visdom.study_app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table
public class Subject {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    @Column(name = "NAME", length = 63)
    private String name;

    @OneToMany(mappedBy="subject", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Schedule> scheduleSet;
}
