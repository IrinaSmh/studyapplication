package tech.visdom.study_app.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.dto.MarkDto;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "MARK")
@Data
public class Mark {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "POINTS")
    private Integer point;

    @Column(name = "ANSWER_ID")
    private Long answerId;

    @Column(name = "COMMENT")
    private String comment;

    public Mark(MarkDto markDto) {
        this.id = markDto.getId();
        this.point = markDto.getPoint();
        this.answerId = markDto.getAnswerId();
        this.comment = markDto.getComment();
    }
}
