package tech.visdom.study_app.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.dto.TaskDto;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table(name = "TASK")
@Data
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "ISANSWERABLE")
    private Boolean isAnswerable;

    @Column(name = "THEME")
    private String theme;

    @Column(name = "WORKTYPE")
    private String workType;

    @Column(name = "LESSON_ID")
    private Long lessonId;



    public Task(TaskDto taskDto) {
        this.id = taskDto.getId();
        this.comment = taskDto.getComment();
        this.isAnswerable = taskDto.getIsAnswerable();
        this.theme = taskDto.getTheme();
        this.lessonId = taskDto.getLessonId();
        this.workType = taskDto.getWorkType();
    }
}
