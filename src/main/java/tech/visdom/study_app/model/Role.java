package tech.visdom.study_app.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
public class Role {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    @Column(name = "NAME", length = 63)
    private String name;

    @Getter
    @Setter
    @Column(name = "CLASS_ID")
    private Long class_id;

}
