package tech.visdom.study_app.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.visdom.study_app.model.Schedule;
import tech.visdom.study_app.model.User;


import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ScheduleDao extends CrudRepository<Schedule, Long> {

    Optional<List<Schedule>> findAllByTeacherAndLessonDateBetween(User teacher, LocalDate fromDate, LocalDate toDate);

    //Optional<List<Schedule>> findAllByClasss
}
