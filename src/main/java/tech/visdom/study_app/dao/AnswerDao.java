package tech.visdom.study_app.dao;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.visdom.study_app.model.Answer;

import java.util.List;
import java.util.Optional;


@Repository
public interface AnswerDao extends CrudRepository<Answer, Long> {

    Optional<Answer> findByTaskIdAndUser_Id(Long taskId, Long studentId);

    Optional<List<Answer>> findAllByTaskId(Long taskId);

    @Query("update Answer a set a.comment=?3 where a.taskId=?1 and a.user.id=?2")
    @Modifying
    void update(Long taskId, Long studentId, String comment);
}
