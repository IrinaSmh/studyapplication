package tech.visdom.study_app.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.visdom.study_app.model.Comment;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentDao extends CrudRepository<Comment, Long> {
    Optional<List<Comment>>findAllByTaskId(Long id);
}
