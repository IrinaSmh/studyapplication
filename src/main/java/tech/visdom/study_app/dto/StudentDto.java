package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.model.User;

@Data
@NoArgsConstructor
public class StudentDto {

    private Long id;

    private String name;

    private String surName;

    private String secondName;

    public StudentDto(User student){
        this.id = student.getId();
        this.name = student.getName();
        this.surName = student.getSurName();
        this.secondName = student.getSecondName();
    }
}
