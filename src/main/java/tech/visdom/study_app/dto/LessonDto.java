package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import tech.visdom.study_app.model.Lesson;

import java.time.LocalDate;
import java.time.LocalTime;


@Data
@NoArgsConstructor
@ToString
public class LessonDto {
    private Long id;

    private LocalDate date;

    private LocalTime time;


    public LessonDto(Lesson lesson) {
        this.id = lesson.getId();
        this.date = lesson.getDate();
        this.time = lesson.getTime();
    }
}
