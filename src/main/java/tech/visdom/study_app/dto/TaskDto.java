package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import tech.visdom.study_app.model.Task;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
@ToString
public class TaskDto {
    private Long id;
    private String comment;
    private Boolean isAnswerable;
    private String theme;
    private String workType;
    private Long lessonId;

    public TaskDto(Task task) {
        this.id = task.getId();
        this.comment = task.getComment();
        this.isAnswerable = task.getIsAnswerable();
        this.theme = task.getTheme();
        this.lessonId = task.getLessonId();
        this.workType = task.getWorkType();
    }
}
