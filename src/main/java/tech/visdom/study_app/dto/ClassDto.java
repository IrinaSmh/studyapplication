package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.visdom.study_app.model.Class;

import javax.persistence.Column;

@Data
@NoArgsConstructor
public class ClassDto {

    private Long id;

    private Integer number;

    private String letter;

    private Integer year;

    public ClassDto(Class classs) {
        this.id = classs.getId();
        this.number = classs.getNumber();
        this.letter = classs.getLetter();
        this.year = classs.getYear();
    }
}
