package tech.visdom.study_app.dto;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.w3c.dom.css.CSSUnknownRule;
import tech.visdom.study_app.model.Subject;

@Data
@NoArgsConstructor
public class SubjectDto {

    private Long id;

    private String name;

    public SubjectDto(Subject subject){
        this.id = subject.getId();
        this.name = subject.getName();
    }
}
