package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.model.Answer;


@Data
@NoArgsConstructor
public class AnswerDto {
    private Long id;

    private String comment;

    private Long taskId;

    private StudentDto studentDto;

    public AnswerDto(Answer answer) {
        this.id = answer.getId();
        this.comment = answer.getComment();
        this.taskId = answer.getTaskId();
        this.studentDto = new StudentDto(answer.getUser());
    }
}
