package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.model.Schedule;


@Data
@NoArgsConstructor
public class ScheduleDto {

    private Long id;

    private ClassDto classDto;

    private LessonDto lessonDto;

    private TeacherDto teacherDto;

    private SubjectDto subjectDto;

    public ScheduleDto(Long id, ClassDto classDto, LessonDto lessonDto, TeacherDto teacherDto, SubjectDto subjectDto) {
        this.id = id;
        this.classDto = classDto;
        this.lessonDto = lessonDto;
        this.teacherDto = teacherDto;
        this.subjectDto = subjectDto;
    }

    public ScheduleDto(Schedule schedule)
    {
        this.id = schedule.getId();
        this.classDto = new ClassDto(schedule.getClasss());
        this.lessonDto = new LessonDto(schedule.getLesson());
        this.teacherDto = new TeacherDto(schedule.getTeacher());
        this.subjectDto = new SubjectDto(schedule.getSubject());
    }
}
