package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.model.Mark;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class MarkDto {

    private Long id;
    @NotNull
    private Integer point;
    @NotNull
    private Long answerId;
    private String comment;

    public MarkDto(Mark mark) {
        this.id = mark.getId();
        this.point = mark.getPoint();
        this.answerId = mark.getAnswerId();
        this.comment = mark.getComment();
    }
}
