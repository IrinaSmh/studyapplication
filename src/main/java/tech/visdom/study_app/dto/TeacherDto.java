package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.model.User;

@Data
@NoArgsConstructor
public class TeacherDto {

    private Long id;

    private String name;

    private String surName;

    private String secondName;

    public TeacherDto(User teacher){
        this.id = teacher.getId();
        this.name = teacher.getName();
        this.surName = teacher.getSurName();
        this.secondName = teacher.getSecondName();
    }
}
