package tech.visdom.study_app.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import tech.visdom.study_app.model.Comment;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class CommentDto {
    private Long id;

    @NotNull
    private String string;

    @NotNull
    private Long taskId;

    @NotNull
    private Long userId;

    public CommentDto(Comment comment) {
        this.id = comment.getId();
        this.string = comment.getString();
        this.taskId = comment.getTaskId();
        this.userId = comment.getUserId();
    }
}
