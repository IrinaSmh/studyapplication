package tech.visdom.study_app.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tech.visdom.study_app.model.Role;
import tech.visdom.study_app.model.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@ToString
public class UserDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @NotNull
    private String name;

    @Getter
    @Setter
    @NotNull
    private String surName;

    @Getter
    @Setter
    @NotNull
    private String secondName;

    @Getter
    @Setter
    @NotNull
    private String login;

    @Getter
    @Setter
    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Getter
    @Setter
    private List<Role> roles;

    public UserDto(User user) {
        id = user.getId();
        name = user.getName();
        surName = user.getSurName();
        secondName = user.getSecondName();
        login = user.getLogin();
        roles = user.getRoles();
    }
}
